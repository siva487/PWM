// Date 12-13 Nov, mob + dim;
// Date 23/Nov (LA_6V_MOB_BTH) Mobile with both SWs + SW on/off debounce + fast SW operation
// 18 Nov, Introduced Mob_Curr_moni for 3Sec monitoring
// Sleep_monitor = 40;	//80 12Nov13
// Date 19Oct, Modifications done for MB, SW and many different combinations 
// Please modify LIB for IOC wake-ups before compiling
// Date 17/Oct. Swapping pin 6 and 10; pin 10 for switch and 6 for LED and MCLR_OFF 
// 2W Chattisgarh, 6V only + No Mobile and 2 ON/OFF SW 
// Modified on Sept 13 
// Used for MoxiDevices(Latest file)
// Fault indication different from MB 
// REF of MB with mobile charge
// Micon : 16F1507
// Date 23May  2013,
// FOSC : Int OSc 16MHz

/* 
   The software supplied herewith by Microchip Technology Incorporated
   (the �Company�) for its PIC� Microcontroller is intended and
   supplied to you, the Company�s customer, for use solely and
   exclusively on Microchip PIC Microcontroller products. The
   software is owned by the Company and/or its supplier, and is
   protected under applicable copyright laws. All rights are reserved.
   Any use in violation of the foregoing restrictions may subject the
   user to criminal sanctions under applicable laws, as well as to
   civil liability for the breach of the terms and conditions of this
   license.

   THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
   WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
   TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
   PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
   IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
   CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
   */
//---------------------------------------------------------------------- 

#include <pic.h>
#include "config.h"
#include "temp_ntc.h"
__CONFIG(FOSC_INTOSC  & BOREN_OFF  & WDTE_ON & CP_ON & PWRTE_ON & MCLRE_OFF);

//----------------------------------------------------------------------
typedef unsigned char byte;
typedef unsigned int word;

#define 	CHR_FREQ		109			//

//----------LED Constant definitions-----------------

#define 	LED_FREQ			109
#define		Con_OS_Volt		OPV_45	

//----------BATTERY constants-------------------------
#define 	Con_LO_BATT BV_LOW
#define 	Con_OK_BATT BV_NOR

//----------IO definitions-------------------------------
#define		LED_CHARGING	LATC4		//changed
#define		LED_LO_BATT		LATB6
#define 	FET_CHG			LATC1
#define		FET_LED			LATC5
#define		FET_LED_OFF		LATC3
#define 	SW1				RA5
#define 	FET_MOB_CHG		LATA2
#define		SW2				RB7			//changed

//----------AD Channels definitions-----------------
#define 	SEL_V_OUT			0b00101101	//AN11
#define 	SEL_I_DRV1			0b00011001	//AN6
#define		SEL_V_BATT			0b00100101	//AN9
#define		SEL_I_BATT			0b00100001	//AN8
#define		SEL_NTC_THR			0b00001101	//AN3
#define		SEL_V_PV			0b00101001	//AN10
#define		SEL_V_FVR			0b01111101	//AN31
#define		SEL_V_MOB			0b00010001	//AN4
#define		SEL_I_MOB			0b00000001	//AN0

//---- bit Variable declarations------------
bit		StartPI;					// Flag to indicate time for PI calculation
bit		CHG_ON;
bit		BATT_LOW;
bit		Deg_HI;

//bit		CURR_25mA_ignore;
//bit		Mob_charging;
//-----------------------------------------------
bit		LED_ON;
bit  	load_fault;
bit  	charge_fault;
byte	charge_fault_count = 0;
//bit 	bit_battery_rest;

//---- Variable declarations------------
int		bit_dusk = 1;
int		dusk_test = 0;
int		junk = 0;
int		bit_PV_IN;
#ifdef DIMMER
int		dimmer_timer;
char		dimmer_hour;
char		dimmer_state; //0 - default, others as defined with 1 == dimm1, 2 == dimm2, 3 == dimm3 levels
#endif
byte	Count;					// Software count for scheduling PI calculation
byte	PWM_off_count;
#ifdef EQUALIZATION
byte day = 0;
#endif
#ifdef MOBILE_MAX_CURRENT_LIMIT
byte	mobile_dch;
byte	mobile_dcl;
word	Mob_current_last;
#endif

word	PV_voltage;
char	fault_count = 0;
word	Battery_voltage;
word	Charge_current;
word	Charge_current_init;
word	crg_loop_ctr;
word	crg_min_ctr;
word	battery_rest_timer;
//word	battery_rest_duration;
int	Sleep_monitor;
byte	charging_mode;
byte	Delta;
byte	monitor_CV_Curr;
word	charge_loop_ctr;
word	soak_monitor;
word	SOAK_TIME;
word	Time_base_Temp;
word	Con_OVR_BATT;
word	Con_99_BATT;
word	Con_95_BATT;
word	Con_92_BATT;
byte	pv_dusk_count;
byte	pv_dawn_count;
//-----------------------------------------------

byte 	Temperature;
byte 	Time_base_us;
byte 	pv_off;
byte	pv_on;
//byte 	Time_base_Key_Press;
word	time_led_blink;
byte	time_load_fault;
word	time_batt_low;
int 	batt_low;
int 	batt_nor;

word	Output_voltage;
word	Output_current;

word	REQ_Setpoint;
byte    PWM_VALUE;

word	Mob_voltage;
word	Mob_current;
byte	Mob_duty_update;

//byte	Battery_connect_OFF;
//byte	Battery_connect_ON;
byte	Batt_PV_count;
byte	PV_count;

//byte	Mob_Curr_moni;
byte	SW_debounce_ON;
byte	SW_debounce_OFF;

//-----Function Prototypes------------
void 	Init_peripherals(void);
void 	Init_BAT_values(void);	
word	read_adc();
void	Get_temp_comp();
void 	delay_ms(byte);
void 	ex_sleep(void);

//---------------------------------------------------------------------
extern 	void 	Init_current(byte);
extern 	byte	CalcPI(word, word, byte);
extern   byte 	Get_Temp(word );
extern	word 	Get_Comp(byte);
extern 	void 	Mob_update(word);
//extern	void 	Prepare_sleep(void);
extern 	byte 	Regulate_voltage(word, word);
extern	byte 	Regulate_current(word);

Prepare_sleep(){
	asm("CLRF 0x12");
	asm("CLRF 0x13");
	asm("BCF 0x1d, 0 ");
	asm("MOVLB 0  ");
	asm("BCF 0x1c, 0x2");
	asm("MOVLB 0x2");
	asm("CLRF 0x17");
	asm("MOVLW 0x48");
	asm("MOVWF 0xb");
	asm("MOVLW 0x20");
	asm("MOVLB 0x7");
	asm("MOVWF 0x12");
	asm("MOVLW 0x20");
	asm("MOVWF 0x11");
	asm("CLRF 0x13");
	asm("MOVLW 0x80");
	asm("MOVWF 0x15");
	asm("MOVLW 0x80");
	asm("MOVWF 0x14");
	asm("CLRF 0x16");
	asm("MOVLB 0xc");
	asm("CLRF 0x19");
	asm("NOP");
	asm("NOP");
	asm("SLEEP");
}
//---------------------------------------------------------------------
// A count variable is incremented in the ISR and a flag is set every
// 4 counts that will trigger the PI calculations. 
//---------------------------------------------------------------------


byte saturated = 0;
int integral = 0;
#define KP 18
#define KI 1
void led_pi(word load_current, int setpoint) {
	long int output1;
	int pi_error;
	pi_error = setpoint - load_current;

	if (pi_error != 0) {
		if (saturated == 0) {
			if (pi_error > 0) {
				if (integral < 32000) {
					integral = integral + pi_error;
				}
			}
			else {
				if (integral > -32000) {
					integral = integral + pi_error;
				}
			}
		}
		output1 = ((pi_error * KP) + (integral * KI));
		//output1 = ((pi_error * 9) + (integral * 1));
		output1 = output1 / 64;
		/*	if((pi_error > 30) && (mobile_stable == 1)){
			output1 = 0;
			integral = 0;
			}*/
		if (output1 > 0x3f8) {
			saturated = 1;
			output1 = 0x3f8;
		}
		else if (output1 < 0x0) {
			saturated = 1;
			output1 = 0;
		}
		else {
			saturated = 0;
		}
		PWM1DCH = (output1 & 0x3FC) >> 2;
		PWM1DCL = (output1 & 0x03) << 6;
#ifdef LED_PI_2_LINE
		PWM3DCH = (output1 & 0x3FC) >> 2;
		PWM3DCL = (output1 & 0x03) << 6;
#endif
	}
}
void interrupt timer2_isr(void)		//Timer 2 interrupt
{
	if(TMR2IF)
	{
		Count++;
		if(Count >= 4)					
		{								
			Count = 0;					//For LED 			=(27uSx4x4)
			StartPI = 1;				//For Battery charging = (27uSx4x4)
		}
		TMR2IF = 0;
	}
}

//-----------------------------------------------------
// Start of main code
//-----------------------------------------------------
void main()
{

	Init_peripherals();

	delay_ms(20);
	//delay_ms(20);
	ADCON0 = SEL_V_BATT;
	Battery_voltage = read_adc();
	
	Init_BAT_values();

	//*****Customizd paramaters options******


	//***********************************		
	while(1)
	{
		//Poll for StartPI flag
		if(StartPI)
		{
#ifdef MAX_LED_CURRENT_LOAD_FAULT_1
			if((Output_current  > MAX_LED_LOAD_CURRENT) && (!load_fault) && (!BATT_LOW))		//150% of Led Current
			{/*LED_ON = 0;*/ fault_count++;
				if(fault_count >= 3){FET_LED_OFF = LED_SWITCH_OFF; PWM1CON = 0; FET_LED = LED_MOS_OFF;PWM3DCH = 0x00; time_load_fault = 40;load_fault = 1; fault_count = 0; Output_current = 0; saturated = 0; integral = 0;}
				//else fault_count = 0;	
			}

#endif
			//Clear the StartPI flag
			StartPI = 0;
			charge_loop_ctr++;
			if(charge_loop_ctr > 2) charge_loop_ctr = 0;
			ADCON0 = SEL_V_PV;
			PV_voltage = read_adc();
			if((PV_voltage > 50) /*&& (bit_dusk == 1)*/){//PV not connected
			//if((PV_voltage > 50) /*|| (Battery_voltage > batt_disc)*/){//PV not connected
				pv_off++;
				pv_on = 0;
				if((pv_off > 150) || (PWM_off_count > 0)) {
					//LED_LO_BATT = 1;
					bit_PV_IN = 1;
					if(time_load_fault == 0)
						LED_CHARGING = 0;
					pv_off = 0;
					if(bit_dusk == 1)
						LED_ON = 0;
					bit_dusk = 0;
					junk = 1;
					//bit_PV_IN = 1; //LED_CHARGING = 1;
					/*CHG_ON = 0;*/PWM4DCH = 0;  PWM4CON = 0x00; FET_CHG = 0;// LED_CHARGING = 0; //Shut off battery charger & put battery charging indicator off
					charge_fault = 0;

				}
			}
			else if ((PV_voltage < 30) || (Charge_current >= Charge_current_init - 10)){//PV may be present
				pv_on++;
				pv_off = 0;
				if(pv_on > 20){
					if(bit_PV_IN == 1){
						if(!load_fault)
							Sleep_monitor = 0;
						bit_PV_IN = 0;
						LED_CHARGING = IND_ON;
						LED_LO_BATT = 0;	
						CHG_ON = 0;
						bit_dusk = 1;
						junk = 0;
						if(LED_ON) {
							FET_LED_OFF = LED_SWITCH_OFF; PWM1DCH = 0x00; PWM1DCL = 0x00; FET_LED = 0;
							PWM1CON = 0x00; //FET_LED = 0;
							PWM3DCH = 0x00; PWM3DCL = 0x0;
						//PWM3CON = 0x00;	
						}
					}
					/*if(dusk_test){
					  dusk_test = 0;
					  pv_off = 200;
					  pv_on = 50;
					  bit_PV_IN = 1; LED_CHARGING = IND_OFF;
					  CHG_ON = 0; PWM4CON = 0x00; FET_CHG = 0;// LED_CHARGING = 1; //Shut off battery charger & put battery charging indicator off
					  }*/
					/*if((Charge_current >= Charge_current_init - 10)){
					//if((1)){
					Batt_PV_count++;
					if(Batt_PV_count > 50){
					dusk_test++;
					if(dusk_test < 5) {pv_off = 200;  pv_on = 50;}
					bit_PV_IN = 1; LED_CHARGING = IND_OFF;
					Batt_PV_count = 0;
					//LED_LO_BATT = 1;	
					//CHG_ON = 0; PWM4CON = 0x00; FET_CHG = 0;// LED_CHARGING = 1; //Shut off battery charger & put battery charging indicator off
					}
					}*/
				}

				}
				else{
					//pv_on = 0;
					//pv_off = 0;
				}
				ADCON0 = SEL_V_BATT;
				Battery_voltage = read_adc();	
				//if((bit_dusk == 1) || (junk == 0) || (bit_PV_IN == 0))
				/*if((LED_ON == 1) && (bit_dusk == 0) && (junk == 1))
				  LED_LO_BATT = 0;	*/
#if 0
				if((PV_voltage < PV_DUSK) && (bit_dusk == 1)){

					pv_dusk_count++;
					pv_dawn_count = 0;
					if( (pv_dusk_count > 200)){
						bit_dusk = 0;
						junk = 1;
#ifdef EQUALIZATION
						day++;
						if(day == EQ_PERIOD){
							Con_OVR_BATT = BV_HI_EQ;
							Get_temp_comp();
							//LED_LO_BATT = IND_ON;
						}
						if(day == EQ_PERIOD + 1){
							Con_OVR_BATT = BV_HI;
							Get_temp_comp();
							day = 0;
						}
#endif
						pv_dusk_count = 0;
						bit_PV_IN = 1; LED_CHARGING = IND_OFF;
						CHG_ON = 0; PWM4DCH = 0; PWM4DCL = 0; PWM4CON = 0x00; FET_CHG = 0; //LED_CHARGING = 1; //Shut off battery charger & put battery charging indicator off
						if((!load_fault) && (!BATT_LOW) && (LED_ON == 1)){
							FET_LED_OFF = LED_SWITCH_ON;
							PWM1DCH = 0x0;
							PWM1CON = 0xC0;
#ifdef LED_PI_2_LINE
							PWM3DCH = 0x0;
							PWM3CON = 0xC0;
#endif
						}
					}
				}
				else if(PV_voltage > PV_DAWN){
					pv_dawn_count++;
					pv_dusk_count = 0;
					if(pv_dawn_count > 200){
						pv_dawn_count = 0;
#ifdef DUSK2DAWN
						bit_dusk = 1;
						junk = 0;
						if(LED_ON) {
							FET_LED_OFF = LED_SWITCH_OFF; PWM1DCH = 0x00; PWM1DCL = 0x00; FET_LED = 0;
							PWM1CON = 0x00; //FET_LED = 0;
							PWM3DCH = 0x00; PWM3DCL = 0x0;
							//PWM3CON = 0x00;	
						}
#endif
						if(PV_voltage > Battery_voltage + 15)
						{
							//		LED_LO_BATT = 0;
							dusk_test = 0;
							PV_count++;
							Batt_PV_count = 0;
							if(PV_count > 50){				
								bit_PV_IN = 0;
								LED_LO_BATT = IND_OFF;	
							}
						}
						else if((PV_voltage <= Battery_voltage + 10) /*&& (Charge_current >= Charge_current_init + 4)*/) {
							PV_count = 0;
							Batt_PV_count++;
							if(Batt_PV_count > 50){
								dusk_test++;
								if(dusk_test < 5) {pv_dusk_count = 200; PV_count = 50;}
								Batt_PV_count = 0;
								bit_PV_IN = 1; LED_CHARGING = IND_OFF;
								//LED_LO_BATT = IND_ON;	
								CHG_ON = 0; PWM4CON = 0x00; FET_CHG = 0; //LED_CHARGING = 1; //Shut off battery charger & put battery charging indicator off
							}

						}
						else{
							PV_count = 0;
							Batt_PV_count = 0;
						}
					}
				}
				else{
					pv_dawn_count = 0;
					pv_dusk_count = 0;
					bit_PV_IN = 1;
					//LED_LO_BATT = 0;	
					CHG_ON = 0; PWM4CON = 0x00; FET_CHG = 0; //LED_CHARGING = 1; //Shut off battery charger & put battery charging indicator off
					//bit_dusk = 1;
				}
#endif
				if(LED_ON == 0)
				{
					REQ_Setpoint = REQ_SP_FULL_12V;
					saturated = 0; integral = 0;
#ifdef DIMMER
					dimmer_timer = 0;
					dimmer_hour = 0;
					dimmer_state = 0;
#endif
					LED_ON = 1;
					BATT_LOW = 0;
					charge_fault_count = 0;
					if(!BATT_LOW){
						FET_LED_OFF = LED_SWITCH_ON;
						PWM1DCH = 0x0; 
#ifdef LED_LOAD_ACTIVE_HIGH
						PWM1CON = 0xC0;
#endif
#ifdef LED_LOAD_ACTIVE_LOW
						PWM1CON = 0xD0;
#endif
#ifdef LED_PI_2_LINE
						PWM3DCH = 0x0; 
						PWM3CON = 0xC0;
#endif
						/*#if defined MOBILE_OPT || defined MOBILE_MOS_AS_SWITCH
						  PWM3DCH = 0x0; 
						  PWM3CON = 0xC0;
#else
PWM3CON = 0x00;
#endif
*/
#if defined MOBILE_MOS_AS_SWITCH
						PWM3DCH = 0x6E;
						PWM3DCL = 0;
#endif
					}
				}

#ifdef DIMMER
				if(dimmer_state == 1){
					REQ_Setpoint = REQ_SP_DIM1_12V;//set it to high
				}
				if(dimmer_state == 2){
					REQ_Setpoint = REQ_SP_DIM2_12V;//set it to high
				}
				if(dimmer_state == 3){
					REQ_Setpoint = REQ_SP_DIM3_12V;//set it to high
				}
#endif

				if(bit_PV_IN == 0)				//Charging routine if PV is present
				{
#ifdef	MOBILE_OPT 
					if(!BATT_LOW) PWM3CON = 0xC0;
#else
					//PWM3DCH = 0x00;
#endif	
					//-----------------------
					if(charge_loop_ctr == 2)		//1mS
					{		 
						CLRWDT();

						NOP(); NOP(); NOP();
						ADCON0 = SEL_I_BATT;
						Charge_current = read_adc();

						if(CHG_ON == 0)
						{
							Charge_current_init = Charge_current;
							if(Battery_voltage >= Con_95_BATT)
								Delta = 1;
							else 
								Delta =  Con_95_BATT - Battery_voltage;

							if(Delta > DELTA_MAX) Delta = DELTA_MAX;		//100mV = 10 minutes

							SOAK_TIME = Delta * 410;		//Delta * 410 x 500/1000/60 = minutes
							//-----------------------

							soak_monitor = 0;		

							PR2 = CHR_FREQ; 					//19.23KHz = 52uS
							PWM4CON = 0xC0;
							PWM_VALUE = 0;
							FET_CHG = 0;					

							Deg_HI = 0;	
							charging_mode = 0;
							CHG_ON = 1;
							//	LED_LO_BATT = 1;

							//Battery_connect_OFF = 0;
							//Battery_connect_ON = 0;
							//CURR_25mA_ignore = 0;

							//Mob_charging = 0;

						}
						/*if(bit_battery_rest == 1){
						//LED_LO_BATT = 1;
						battery_rest_timer++;
						PWM_VALUE = 0;
						LED_CHARGING = IND_OFF;
						if(battery_rest_timer >= battery_rest_duration){
						bit_battery_rest = 0;
						crg_loop_ctr = 0;
						crg_min_ctr = 0;
						battery_rest_timer = 0;
						}
						}*/
						if(charging_mode == 0)
						{ 
							PWM_VALUE = 0x6E; //Regulate_current(Charge_current);

							if(Battery_voltage < Con_OVR_BATT) 			
							{
								soak_monitor = 0;
								crg_loop_ctr++;
								if((crg_loop_ctr >= 4590) /*&& (bit_battery_rest == 0)*/){ //1 min
									crg_loop_ctr = 0;
									crg_min_ctr++;
#ifdef RDS_TEMP_CONTROL
									/*if((crg_min_ctr & 0x1FF) == 0)*/{
										if((PV_voltage - Battery_voltage) > 8){
											LED_LO_BATT = IND_ON;
											//	bit_battery_rest = 1;
											battery_rest_timer = 0;

										}
									}
#endif
									if(crg_min_ctr > BATTERY_REST_GAP){
										//battery_rest_duration = BATTERY_REST_DURATION;
										battery_rest_timer = 0;
										//bit_battery_rest = 0;
									}
								}
								/*if(bit_battery_rest == 1){
								//LED_LO_BATT = 1;
								battery_rest_timer++;
								PWM_VALUE = 0;
								if(battery_rest_timer >= BATTERY_REST_DURATION){
								bit_battery_rest = 0;
								crg_loop_ctr = 0;
								crg_min_ctr = 0;
								battery_rest_timer = 0;

								}
								}*/
							}
							else 
							{
								soak_monitor++;
								if(soak_monitor >= 2 * BATTERY_REST_DURATION)				//500MSecs
									if(soak_monitor >= 5)				//500MSecs
									{
										charging_mode = 1; soak_monitor = 0; monitor_CV_Curr = 0;
										//Battery_connect_OFF = 0;
										//Battery_connect_ON = 0;
										//CURR_25mA_ignore = 0;
										//battery_rest_duration = CV_BATTERY_REST_DURATION;
										crg_loop_ctr = 0;
										crg_min_ctr = 0;
										battery_rest_timer = 0;
										//bit_battery_rest = 1;
									}
							}
							//	if(Charge_current < CURR_25mA)
							{
								time_led_blink++;
								if(time_led_blink >= 1000)
								{	
									LED_CHARGING = !LED_CHARGING; 	//Blink LED, 1Sec
									time_led_blink=0; 					//
								}
							}
							//	else {LED_CHARGING = 1;}
						}
						else if(charging_mode == 1)
						{
							/*if(bit_battery_rest == 1){
							  battery_rest_timer++;
							  PWM_VALUE = 0;
							  if(battery_rest_timer >= CV_BATTERY_REST_DURATION){
							  bit_battery_rest = 0;
							  battery_rest_timer = 0;
							  }
							  }else*/{
								  if(Battery_voltage < Con_92_BATT){
									  PWM_VALUE = 0x6E; //Regulate_current(Charge_current);
									  //charging_mode = 0;
								  }
								  else if(Battery_voltage > Con_99_BATT){
									  //bit_battery_rest = 1;
									  PWM_VALUE = 0;
								  }
								  /*else
								    PWM_VALUE = Regulate_voltage(Battery_voltage, Con_99_BATT);*/
							  }


						time_led_blink++;
						if(time_led_blink >= 555)					//500mS
						{	
							time_led_blink=0; 					//
							LED_CHARGING = !LED_CHARGING; 	//Blink LED, 1Sec
							//---------------------------------------------------------
							/*	Battery_connect_ON++;
								if(Battery_connect_ON >= 20)
								{
								PWM_VALUE = 0;	
								FET_CHG = 0;
								CURR_25mA_ignore = 1;	
								Battery_connect_OFF++;
								if(Battery_connect_OFF >= 4)
								{
								Battery_connect_OFF = 0;
								Battery_connect_ON = 0;
								CURR_25mA_ignore = 0;
								}		
								}*/
							//---------------------------------------------------------
							/*if((Charge_current >= CURR_25mA) && !CURR_25mA_ignore)
							  {
							  monitor_CV_Curr++;
							  if(monitor_CV_Curr >= 100)		//5Sec
							  {
							  monitor_CV_Curr = 0;
							  soak_monitor = SOAK_TIME;	
							  }
							  }
							  else monitor_CV_Curr = 0;*/

							soak_monitor++;
							if(soak_monitor >= SOAK_TIME){
								charging_mode = 2;
								//battery_rest_duration = CV_BATTERY_REST_DURATION;
								battery_rest_timer = 0;
								soak_monitor = 0;
								//FET_CHG = 0;
								//bit_battery_rest = 1;
							}
						}		
						}
						else
						{
							LED_CHARGING = IND_ON;
							LED_LO_BATT = IND_OFF;
							LED_LO_BATT = 0;
							/*if(bit_battery_rest == 1){
							  battery_rest_timer++;
							  PWM_VALUE = 0;
							  if(battery_rest_timer >= CV_BATTERY_REST_DURATION){
							  bit_battery_rest = 0;
							  crg_loop_ctr = 0;
							  crg_min_ctr = 0;
							  }
							  }
							  else*/{

								  /*if(Battery_voltage < Con_92_BATT){
								    charging_mode = 0;
								    }
								    else*/{
#ifdef FLOAT_CHARGING
									    if(Battery_voltage < Con_95_BATT){
										    PWM_VALUE = 0x6E; //Regulate_current(Charge_current);
										    //charging_mode = 0;
									    }
									    else if(Battery_voltage > Con_92_BATT){
										    //bit_battery_rest = 1;
										    PWM_VALUE = 0;
									    }
									    //PWM_VALUE = Regulate_voltage(Battery_voltage, Con_95_BATT);
#else
									    charging_mode = 0;
#endif
								    }
							  }
						}
#ifdef HIGH_TEMP_CHARGING_OFF
						if(Deg_HI)
						{
							PWM_VALUE = 0;
							FET_CHG = 0;
							if(Temperature < NOR_TEMP) 
								Deg_HI = 0;
						}	
#endif						
						if(Temperature >= HIGH_TEMP)
						{
							Deg_HI = 1;
						}

						//if(((charging_mode != 0) && (Charge_current < CHARGING_MAX_CV)) || (Battery_voltage > BATTERY_HIGH_OFF)){
						if(/*((charging_mode != 0) && (Charge_current < CHARGING_MAX_CV)) ||*/ (Battery_voltage > BATTERY_HIGH_OFF)){
							PWM_off_count = CHARGING_OFF_LOOP_CNT;
						}
						if(PWM_off_count){
							PWM_off_count--;
							PWM_VALUE = 0;
						}
#ifdef	CHARGING_ERROR
						if(Charge_current < MAX_CHARGING_CURRENT){ //Greater then 15A
							charge_fault_count++;
							if(charge_fault_count > 6){
								bit_PV_IN = 1; LED_CHARGING = IND_OFF;
								CHG_ON = 0; PWM4DCH = 0; PWM4DCL = 0; PWM4CON = 0x00; FET_CHG = 0;
								charge_fault = 1;
								charge_fault_count = 0;
								//time_load_fault = 200;
								Sleep_monitor = SLEEP_TIMER;
							}
						}
#endif
						//---------------------------------------		
#ifdef TEMP_CONTR
						Time_base_Temp++;
						if(Time_base_Temp >= 1000)							//Approx 10Sec  = 1mS*10000
						{	
							PWM_off_count = 15;
							pv_off = 250;
							PWM_VALUE = 0;
							Get_temp_comp();
							Time_base_Temp = 0;
						}
						PWM4DCH = PWM_VALUE;
						PWM4DCL = 0;
#endif
					}
				}

				//LED Light routine
#ifdef DUSK2DAWN
				if((LED_ON == 1) && (bit_dusk == 0) && (junk == 1))
#else
					if(LED_ON == 1) 
#endif
					{
						//LED_LO_BATT = 0;
						if(charge_loop_ctr == 1)		//1mS
						{
							// Read ADC to get current feedback value.  The present
							// sample is averaged with the previous sample. 
							ADCON0 = SEL_I_DRV1;
							Output_current = read_adc();
							if((!load_fault) && (!BATT_LOW))
							{
								/*#ifdef MOBILE_MOS_AS_SWITCH
								  PWM3CON = 0XC0;
								  PWM3DCH = 0X6E;
								  PWM3DCL = 0;
#endif	*/
#ifdef LED_PI
#ifdef EXTRA_HI_BATT_PROT
								if(Battery_voltage >= MAX_BATT_VOLT_LED_LOAD){
									PWM1CON = 0; PWM1DCH = 0; PWM1DCL = 0; FET_LED = LED_MOS_OFF; FET_LED_OFF = LED_SWITCH_OFF; /*LED_ON = 0*/;time_load_fault = 200;load_fault = 1;PWM3CON = 0; }
								else
#endif

									// Calculate the PI controller using variables in the 'Current' data structure.
									led_pi(Output_current, REQ_Setpoint);
								//PWM_VALUE	= CalcPI(Output_current, REQ_Setpoint, 250);

								// Write the calculated PWM duty cycle to the CCP module.
								//PWM1DCH = (PWM_VALUE >> 2);
								//PWM1DCL =  (PWM_VALUE << 6);
#endif
							}

							ADCON0 = SEL_V_OUT;
							Output_voltage = read_adc();
#ifndef NO_OVER_VOLTAGE_PROTECTION
							if(Output_voltage >= Con_OS_Volt)									//36Vfor 12V battey and 18V for 6V battery
							{PWM1CON = 0; PWM1DCH = 0; PWM1DCL = 0; FET_LED = LED_MOS_OFF; FET_LED_OFF = LED_SWITCH_OFF; /*LED_ON = 0*/;time_load_fault = 200;load_fault = 1;
								PWM3DCH = 0x0; PWM3DCL = 0x0;}		//disable LED PWM
#endif		
#ifdef SHORT_VOLTAGE_PROTECTION
							if(Output_voltage < SHORT_VOLT)									//36Vfor 12V battey and 18V for 6V battery
							{PWM1CON = 0; PWM1DCH = 0; PWM1DCL = 0; FET_LED = LED_MOS_OFF;time_load_fault = 200;load_fault = 1; FET_LED_OFF = LED_SWITCH_OFF; PWM3DCH = 0x0;}		//disable LED PWM
#endif	
							//-------------------------------------------------------
#ifdef MAX_LED_CURRENT_LOAD_FAULT
							if((Output_current  > MAX_LED_LOAD_CURRENT) && (load_fault == 0) && (!BATT_LOW))		//150% of Led Current
							{/*LED_ON = 0;*/ fault_count++;
								if(fault_count >= 3){
									FET_LED_OFF = LED_SWITCH_OFF; 
									PWM1DCH = 0; PWM1CON = 0; FET_LED = LED_MOS_OFF;
#ifdef LED_PI_2_LINE
									PWM3DCH = 0; PWM3CON = 0; FET_MOB_CHG = LED_MOS_OFF;
#endif
									time_load_fault = 40;load_fault = 1; fault_count = 0;}
								//else fault_count = 0;	
							}
#endif
							Time_base_us++;
							if(Time_base_us >= 111)				//50mS routine	
							{
								Time_base_us = 0;
#ifdef DIMMER
								dimmer_timer++;
								if(dimmer_timer >= DIMMER_CYCLES_PER_HOUR){
									dimmer_hour++;
									dimmer_timer = 0;
									if((dimmer_hour >= DIMMER_HOURS_1) && (dimmer_state == 0)){
										dimmer_hour = 0;
										dimmer_state = 1;
									}
									if((dimmer_hour >= DIMMER_HOURS_2) && (dimmer_state == 1)){
										dimmer_hour = 0;
										dimmer_state = 2;
									}
									if((dimmer_hour >= DIMMER_HOURS_3) && (dimmer_state == 2)){
										dimmer_hour = 0;
										dimmer_state = 3;
									}
								}
#endif

								CLRWDT();


								/*	if(BATT_V == 0)
									{
									ADCON0 = SEL_V_FVR;
									if(read_adc() > 560) Battery_voltage = 0;
									}	*/

								if((Battery_voltage < Con_LO_BATT) && (BATT_LOW == 0) && (!load_fault))		//check for LOW Battery voltage
								{
									BATT_LOW = 1; LED_LO_BATT = IND_ON;	//light LOW battery indication
									FET_LED_OFF = LED_SWITCH_OFF;
									PWM1CON = 0x0; FET_LED = 0;
									PWM1DCH = 0x00; PWM1DCL = 0x00;
									PWM3DCH = 0x0; PWM3DCL = 0x0;
									time_batt_low = 1000; 								//6Sec
								}
								else 
								{
									if(BATT_LOW == 1)
									{
										if((Battery_voltage > Con_OK_BATT) && (!load_fault))
										{
											BATT_LOW = 0;
											LED_LO_BATT = IND_OFF;
											time_batt_low=0; /*PWM3CON = 0xC0*/;
											/*FET_LED_OFF = LED_SWITCH_ON;
											  PWM1DCH = 0x0;
											  PWM1DCL = 0x0;
											  PWM1CON = 0xC0;*/
											LED_ON = 0;
										}
									}
								}	
								//-------------------------------------------------------
								/*if(load_fault)
								  {
								  if(SW1 == SW2 == 1)
								  {load_fault = 0;LED_LO_BATT = 1; LED_CHARGING = 1;}
								  }*/
								//-------------------------------------------------------
								if(time_batt_low)
								{	
									//if(SW1 == SW2 == 1) time_batt_low = 1;

									LED_CHARGING = IND_OFF;
									time_batt_low--;
									if(time_batt_low)
									{
										time_led_blink++;
										if(time_led_blink >= 5)
										{	
											LED_LO_BATT = !LED_LO_BATT; time_led_blink=0; 		//
										}
									}
									else 
									{
										LED_LO_BATT = IND_OFF; 
									}
								}
								else if(time_load_fault)
								{
									time_led_blink++;
									if(time_led_blink >= 5)
									{	
										LED_CHARGING = !LED_CHARGING; 
										LED_LO_BATT = LED_CHARGING; 
										time_led_blink=0;
									}
									time_load_fault--; 
									if(time_load_fault == 0) {LED_LO_BATT = IND_OFF; LED_CHARGING = IND_OFF;}
								} 
								//-------------------------------------------------------
								else if(load_fault)
								{
									Sleep_monitor = SLEEP_TIMER;
									//Sleep_monitor++;
									//PWM3DCH = 0x00;		//Put Mob charging OFF
									//Mob_charging = 0;
								}
								else if(SW1 != SW2)
								{	
#ifdef	MOBILE_OPT			
									ADCON0 = SEL_I_MOB;
									Mob_current = read_adc();
									if(Mob_current > MOB_CURRENT_HIGH_OFF)			//50mA
									{
										Mob_charging = 1;
										Mob_Curr_moni = 0;
										Sleep_monitor = 0;
										if(!SW1)
										{
											PWM1CON = 0x00; FET_LED = LED_MOS_OFF;	//mob charging started put Hi mode off							
										}
									}
									else
									{
										if(Mob_charging)
										{
											Mob_Curr_moni++;
											if(Mob_Curr_moni >= 40)		//2Secs
											{
												Mob_charging = 0;
												Mob_Curr_moni = 0;							
												if(SW2)
												{
													Sleep_monitor = 80;		//12Nov13
													WDTCON = 0x11;			//256mSec
												}
											}
										}
										else 
											Sleep_monitor = 0;	
									}
#else
									Sleep_monitor = 0;	
#endif
								}
							}
							//-------------------------------------------------------
							//			if(CHG_ON) {CHG_ON = 0; PWM4CON = 0x00; FET_CHG = 0; LED_CHARGING = 1;}	//Shut off battery charger & put battery low off indication	
							//-------------------------------------------------------
						}
					}//end of LED drive routine

				//FET_LED_OFF = LED_ON;
#ifdef LED_PV_IDLE_POWER_MGMT
				//if((LED_ON == 0) && (bit_PV_IN == 1))
				if(((bit_dusk == 1) && (bit_PV_IN == 1) && (junk == 0) && (PV_voltage < BV_NOR)) || ((LED_ON == 0) && (bit_PV_IN == 1)))
				{
					Sleep_monitor++;
				}
#endif			
				//-----------------------------------------------------------
#ifdef POWER_MGMT
				if(Sleep_monitor >= SLEEP_TIMER)		//4Secs
				{
					Sleep_monitor = 0;
					//Mob_charging = 0;
					//Mob_Curr_moni = 0;
					load_fault = 0; charge_fault = 0;  LED_LO_BATT = IND_OFF; LED_CHARGING = IND_OFF;	
					ex_sleep();
				}	
#endif			
#ifdef	MOBILE_OPT 

				//			Mob_duty_update++;
				//			if(Mob_duty_update >= 2)				//450x2 ~ 2mS
				if(charge_loop_ctr == 0)		//1mS
				{
					//				Mob_duty_update = 0;
					/*				if(bit_PV_IN == 1)
									{	
									if(BATT_LOW) {PWM3CON = 0x00; PWM3DCH = 0; PWM3DCL = 0;}
									}*/
					ADCON0 = SEL_V_MOB;
					Mob_voltage = read_adc();
#ifdef MOBILE_MAX_CURRENT_LIMIT
					if(Mob_current < MOBILE_MAX_CURRENT)
					{
						if(Mob_current > Mob_current_last){
							Mob_current_last = Mob_current;
							mobile_dcl = PWM3DCL;
							mobile_dch = PWM3DCH;
						}
						Mob_update(Mob_voltage);
					}
					else{
						PWM3DCL = mobile_dcl;
						PWM3DCH = mobile_dch;

					}
#else
					Mob_update(Mob_voltage);
#endif
					Mob_voltage = 1;
				}
#endif
				//-------------------------------------------------------
			}//StartPI routine
		}//while	
	}

	//-------------------------------------------------------
	void ex_sleep(void)
	{
re_sleep:
		Prepare_sleep();

		Init_peripherals();

		delay_ms(20);

		//Init_BAT_values();

		//--------------------------------------------
		if(PCONbits.nRWDT == 0)
		{
			PCONbits.nRWDT = 1;
		}
		ADCON0 = SEL_V_PV;
		PV_voltage = read_adc();
		//if((PV_voltage > PV_DUSK) && (PV_voltage < PV_DAWN))
		if((PV_voltage > 30) && (PV_voltage < 50) )
		{
			goto re_sleep;	
		}
	}
	//---------------------------------------------------------------------
	void Init_peripherals()
	{
		OSCCONbits.IRCF = 0xF;			//16MHz operation		
		WDTCON = 0x1B;					//8Sec

		INTCON = 0x00;
		IOCAN = 0x00;
		IOCAP = 0x00;	
		IOCAF = 0x00;

		IOCBN = 0x00;
		IOCBP = 0x00;
		IOCBF = 0x00;
#ifdef LED_PI_2_LINE
		ANSELA = 0x13;
		LATA = 0x00;
		TRISA =  0xFB;
#else
		ANSELA = 0x13;
		LATA = 0x00;
		TRISA =  0xFB;
#endif
		ANSELB = 0x30;
		LATB = 0x80 | IND_OFF << 6;
		TRISB =  0xBF;	

		ANSELC = 0xC5;
#ifdef LED_LOAD_ACTIVE_HIGH
		LATC = 0x0 | IND_OFF << 4 ;			
#endif	
#ifdef LED_LOAD_ACTIVE_LOW
		LATC = 0x30;		
#endif	
		TRISC =  0xC5;

#ifdef INTERNAL_ADC_REFERENCE
		ADCON1 = 0xD0;				//Internal Ref 2.048V = Reso 2mV, Right Justified
#else
		ADCON1 = 0xDE;				//External Ref 2.048V = Reso 2mV, Right Justified
#endif
		ADON = 1;

		T2CON = 0x18;				//PR 1:1. PS 1:4
		PR2 = LED_FREQ;

		FVRCON = 0x81;
		while(FVRCONbits.FVRRDY == 0);
		//--------------------------------
		//--------------------------------
		LED_ON = 0;
		CHG_ON = 0;
		ADCON0 = SEL_V_BATT;
		Battery_voltage = read_adc();
		ADCON0 = SEL_V_PV;
		PV_voltage = read_adc();
		/*if(PV_voltage < PV_DUSK){	
			FET_CHG = 0;
			bit_PV_IN = 1;
		}
		else if(PV_voltage > Battery_voltage){
			FET_CHG = 1;
			bit_PV_IN = 0;
		}*/

		//PWM3DCH = 0x00;		//
		//PWM3DCL = 0;
		/*
#if	defined	MOBILE_OPT || defined MOBILE_MOS_AS_SWITCH
PWM3CON = 0xC0;
#else
PWM3CON = 0x00;
#endif
*/
		TMR2IF = 0;
		TMR2ON = 1;

		TMR2IE = 1;
		PEIE = 1;
		GIE = 1;
	}

	//---------------------------------------------------------------------
	void Init_BAT_values()
	{
		
		Con_OVR_BATT = BV_HI;			//14.4V
		Get_temp_comp();
	}
	//---------------------------------------------------------------------
	word read_adc(void)
	{
		byte loop=0;
		word tmp=0;
		word data;
		for(; loop<4; loop++)
		{
			ADGO = 1;
			while( ADGO ) {}
			data = ADRESH;
			data = data << 8;
			data = (data + ADRESL) & 0x3FF;	
			tmp += data;
		}
		tmp = tmp >> 2;
#ifdef INTERNAL_ADC_REFERENCE
		tmp *= 5;
		tmp = (tmp >> 1) & 0x3FF;
#endif
		return (tmp);
	}

	//---------------------------------------------------------------------
	void delay_ms(byte del)
	{
		word	i;
		while(del)
		{	
			i=385;		
			while(i){i--;}
			del--;
		}
	}
	//-------------------------------------------------------
	void Get_temp_comp(void)
	{
		int i;
		word 	Ther_voltage;

		ADCON0 = SEL_NTC_THR;
		Ther_voltage = read_adc();		
#ifdef TEMP_COMP_OLD
		Temperature = Get_Temp(Ther_voltage) - TEMP_OFF;
		Con_OVR_BATT = BV_HI + 25 - Temperature;//This value is applicable only with current resistance bridge and calculation as per internal adc;
#endif
#ifdef TEMP_COMP_OFF
		Con_OVR_BATT = Con_OVR_BATT + BV_HI - 458;
#endif
#ifdef TEMP_COMP
		Con_OVR_BATT = BV_HI + 25;//This value is applicable only with current resistance bridge and calculation as per internal adc;
#endif
#ifdef TEMP_COMP
		for(i=0;i<9;i++)
		{
			if(Ther_voltage <=temperature_ntc[i])
			{
				Temperature = i* 10;

				break;
			}
			Con_OVR_BATT = Con_OVR_BATT - 10;//This value is applicable only with current resistance bridge and calculation as per internal adc;
			if(i == 9)// if temp>100
				Temperature = i* 10;
		}
#endif

		Con_99_BATT = (Con_OVR_BATT * 99)/100;
		Con_95_BATT = (Con_OVR_BATT * 95)/100; 
		Con_92_BATT = (Con_OVR_BATT * 97)/100; 

	}
