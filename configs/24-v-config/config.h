#ifndef __CONFIG_H__
#define __CONFIG_H__
/*****************************************************************************
 * BOARD SUPPORTED: M2756-S-P.
Output cap C200:330uF/63V.
Input cap C202:100uF/50V.
Inductor 47uH(big 15/18).
MOB mosfet and BJT with change in 47K and 10K.
 * **************************************************************************/
/*****************************************************************************
 * Revision history:
 *Date: 21/04/2015
 *Comment: Created first draft of this file. Standard file to be used with Moxie
 *standard 12V_7AH_9W_6W(18V).
 *Author: Siva
 * **************************************************************************/
/******************************************************************************
  PWM Settings
  RC5/PWM1 - LED PWM
  RC3/PWM2 - LED Switch
  RC1/PWM4 - Charging
 ******************************************************************************/

/******************************************************************************
  LED LOAD PI SETTING
  Shunt Resistance:   1.2||1.2||1.2 = 0.4ohm
  Over voltage protection: resistance bridge 120k, 1.8K 
 ******************************************************************************/
#define		REQ_SP_FULL_12V  	100 //150		
//~80mA 18
#define		REQ_SP_DIM1_12V		63	//~40mA 36		
#define		REQ_SP_DIM2_12V		63	//~40mA 36		
#define		REQ_SP_DIM3_12V		63	//~40mA 36		
#define		OPV_45			450		//35V	
#define		SHORT_VOLT		15
#define 	EXTRA_HI_BATT_PROT
#define 	MAX_BATT_VOLT_LED_LOAD   462
//#define SHORT_VOLTAGE_PROTECTION
#define SHORT_VOLTAGE_PROTECTION_VERY_SHORT
#define	VERY_SHORT_VOLT 
#define MAX_LED_CURRENT_LOAD_FAULT
#define MAX_LED_LOAD_CURRENT	240
#define LED_PI
#define LED_PI_2_LINE
#define LED_LOAD_ACTIVE_HIGH
#define LED_SWITCH_ON	1
#define LED_SWITCH_OFF	0
#define LED_MOS_ON	1
#define LED_MOS_OFF	0
//#define MOBILE_MOS_AS_SWITCH

/******************************************************************************

  INDICATOR
 ******************************************************************************/
#define IND_ON 1	// for street Light IND_ON 1.
#define IND_OFF 0       // for lantern IND_ON 0.
/******************************************************************************/

/******************************************************************************
  BATTERY SETTING
  Battery voltage: Resistance bridge 75K, 5.1K
  Charging current resistance: .22||.22||.22 1/4 watt each
 ******************************************************************************/
#define BATT_12V
//#define RDS_TEMP_CONTROL
#define DELTA_BATT +0
#ifdef BATT_12V
#define		BV_LOW		360 + DELTA_BATT 	//363		//330
#define		BV_NOR		400 + DELTA_BATT			//375				//12V
#define		BV_HI		460	+ DELTA_BATT		//75K and 5K1
#define		BATTERY_HIGH_OFF 472	
//#define EQUALIZATION
#define EQ_PERIOD	30
#define		BV_HI_EQ		500	+ DELTA_BATT		//75K and 5K1
#define		CHR_CURR	2
#define		CHARGING_MAX_CURRENT 125
#define		CHARGING_MAX_CV 0xFA
#define		DELTA_MAX	32			//1 means 5 min second stage
//#define		BATTERY_REST_DURATION	5000			//1000 means 1.3 sec
#define		BATTERY_REST_DURATION	0			//1000 means 1.3 sec
#define		CV_BATTERY_REST_DURATION	15000			//1000 means 1.3 sec
#define		BATTERY_REST_GAP	10			//1 means 1 min
#define FLOAT_CHARGING
#endif

#ifdef BATT_11_1V
#define		BV_LOW		302 + DELTA_BATT 	//363		//330
#define		BV_NOR		339 + DELTA_BATT			//375				//12V
#define		BV_HI			410	+ DELTA_BATT		//75K and 5K1
#define		BATTERY_HIGH_OFF 420	
//#define EQUALIZATION
#define EQ_PERIOD	30
#define		BV_HI_EQ		500	+ DELTA_BATT		//75K and 5K1
#define		CHR_CURR	2
#define		CHARGING_MAX_CURRENT 125
#define		CHARGING_MAX_CV 0xFA
#define		DELTA_MAX	32			//1 means 5 min second stage
//#define		BATTERY_REST_DURATION	5000			//1000 means 1.3 sec
#define		BATTERY_REST_DURATION	0			//1000 means 1.3 sec
#define		CV_BATTERY_REST_DURATION	15000			//1000 means 1.3 sec
#define		BATTERY_REST_GAP	10			//1 means 1 min
#define FLOAT_CHARGING
#endif

#ifdef BATT_7_4V
#define		BV_LOW		205 + DELTA_BATT 	//363		//330
#define		BV_NOR		247 + DELTA_BATT			//375				//12V
#define		BV_HI			273	+ DELTA_BATT		//75K and 5K1
#define		BATTERY_HIGH_OFF 283	
//#define EQUALIZATION
#define EQ_PERIOD	30
#define		BV_HI_EQ		500	+ DELTA_BATT		//75K and 5K1
#define		CHR_CURR	2
#define		CHARGING_MAX_CURRENT 125
#define		CHARGING_MAX_CV 0xFA
#define		DELTA_MAX	32			//1 means 5 min second stage
//#define		BATTERY_REST_DURATION	5000			//1000 means 1.3 sec
#define		BATTERY_REST_DURATION	0			//1000 means 1.3 sec
#define		CV_BATTERY_REST_DURATION	15000			//1000 means 1.3 sec
#define		BATTERY_REST_GAP	10			//1 means 1 min
#define FLOAT_CHARGING
#endif

#ifdef BATT_6V
#define		BV_LOW	176			//330
#define		BV_NOR	196			//375				//12V
#define		BV_HI	228			//75K and 5K1
#define		CHR_CURR	2
#define		CHARGING_MAX_CURRENT 125
#endif
#define 	CHARGING_LOOP	1	
/******************************************************************************
PV MANAGEMENT
PV volate: Resistance bridge: 33K and 1K
 ******************************************************************************/
#define PV_DAWN 260
#define PV_DUSK 97

/******************************************************************************
CHARGING MANAGEMENT
MOSFETS: CETP30p03 and 2 SSM3J334. 
charging current of 2.75A.
tinning to be done.
Diodes to be mounted 3 5819.
 ******************************************************************************/
#define CURR_25mA	250
#define CHARGING_OFF_LOOP_CNT 300

/******************************************************************************
Mobile MANAGEMENT
current control: .5ohm, half watt
 ******************************************************************************/
//#define 	MOBILE_OPT
#define MOB_CURRENT_HIGH_OFF	40
//#define MOBILE_MAX_CURRENT_LIMIT
#define MOBILE_MAX_CURRENT	50

/******************************************************************************
POWER MANAGEMENT
 ******************************************************************************/
#define POWER_MGMT
#define LED_PV_IDLE_POWER_MGMT
#define SLEEP_TIMER	1000

/******************************************************************************
 ADC Reference
 ******************************************************************************/
#define INTERNAL_ADC_REFERENCE

/******************************************************************************
 Temperature Compensation
 ******************************************************************************/
#define TEMP_COMP
#define TEMP_CONTR
#define HIGH_TEMP	70
#define NOR_TEMP	55
#define TEMP_OFF 	7
#define HIGH_TEMP_CHARGING_OFF
/******************************************************************************
 Over voltage protection
 ******************************************************************************/
//#define NO_OVER_VOLTAGE_PROTECTION

#define DUSK2DAWN
//#define DIMMER
//#define DIMMER_CYCLES_PER_HOUR	 50
#define DIMMER_CYCLES_PER_HOUR 	25700 
#define	DIMMER_HOURS_1	6
#define	DIMMER_HOURS_2	4
#define	DIMMER_HOURS_3	4
#endif
